package ir.hamsarin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HamsarinApplication {

    public static void main(String[] args) {
        SpringApplication.run(HamsarinApplication.class, args);
    }

}
