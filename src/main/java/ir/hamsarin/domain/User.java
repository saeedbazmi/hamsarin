package ir.hamsarin.domain;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class User {
    @Id
    private Long id;
    private String username;
    private String password;
    private String name;
    private String family;
    private String bio;
    private String nationalID;
    private Long FK_CountryID;
    private Long FK_StateID;
    private Long FK_CityID;
    private Long FK_RegionID;



}
