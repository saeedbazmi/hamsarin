package ir.hamsarin.domain;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class UserDetails {
    @Id
    private Long id;
    private Integer age;
    private Integer weight;
    private Integer height;
    private Integer sibilingNumber;
    private boolean smoke;
    private boolean alcohol;
    private boolean drugUse;
    private String job;
    private Long FK_UserID;
    private Long FK_SkinColorID;
    private Long FK_HairColorID;
    private Long FK_SalaryID;
    private Long FK_HouseTypeID;
    private Long FK_HourseAreaID;
    private Long FK_EducationDegreeID;
    private Long FK_ReligionID;
    private Long FK_EthicID;

}
