package ir.hamsarin.domain;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;

@Entity
public class Country {
    @Id
    @JoinColumn()
    private Long id;
    private String countryName;
}
