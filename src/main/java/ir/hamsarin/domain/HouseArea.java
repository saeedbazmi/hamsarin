package ir.hamsarin.domain;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class HouseArea {
    @Id
    private Long id;
    private Long houseArea;
}
