package ir.hamsarin.domain;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Plan {
    @Id
    private Long id;
    private Long planName;
    private Long planPrice;
}
