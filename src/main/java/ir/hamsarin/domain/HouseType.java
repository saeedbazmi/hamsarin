package ir.hamsarin.domain;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class HouseType {
    @Id
    private Long id;
    private String houseType;
}
