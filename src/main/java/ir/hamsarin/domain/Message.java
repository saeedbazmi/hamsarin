package ir.hamsarin.domain;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Message {
    @Id
    private Long id;
    private String messageBody;
    private String messageDate;
    private String RecordTime;
    private String messageHour;
    private String messageMinute;
    private String messageSecond;
    private Long FK_UserID_Sender;
    private Long FK_UserID_Receiver;
}
